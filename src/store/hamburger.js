import { defineStore } from "pinia";

export const useHamburgerStore = defineStore("hamburger", {
  state: () => ({
    data: [
      {
        id: "1",
        title: "经典汉堡包",
        desc: "百分百纯牛肉配搭爽脆酸瓜洋葱粒与美味番茄酱经典滋味让你无法抵挡！",
        price: 12,
        img: "/images/meals/1.png"
      },
      {
        id: "2",
        title: "双层吉士汉堡",
        desc: "百分百纯牛肉与双层香软芝，加上松软面包及美味酱料，诱惑无人能挡！",
        price: 20,
        img: "/images/meals/2.png"
      },
      {
        id: "3",
        title: "巨无霸汉堡",
        desc: "两块百分百纯牛肉，搭配生菜、洋葱等新鲜食材，口感丰富，极致美味！",
        price: 24,
        img: "/images/meals/3.png"
      },
      {
        id: "4",
        title: "麦辣鸡腿汉堡",
        desc: "金黄脆辣的外皮，鲜嫩幼滑的鸡腿肉，多重滋味，一次打动您挑剔的味蕾！",
        price: 21,
        img: "/images/meals/4.png"
      },
      {
        id: "5",
        title: "板烧鸡腿堡",
        desc: "原块去骨鸡排嫩滑多汁，与翠绿新鲜的生菜和香浓烧鸡酱搭配，口感丰富！",
        price: 22,
        img: "/images/meals/5.png"
      },
      {
        id: "6",
        title: "麦香鸡汉堡",
        desc: "清脆爽口的生菜，金黄酥脆的鸡肉。营养配搭，好滋味的健康选择！",
        price: 14,
        img: "/images/meals/6.png"
      },
      {
        id: "7",
        title: "吉士汉堡包",
        desc: "百分百纯牛肉与香软芝士融为一体配合美味番茄醬丰富口感一咬即刻涌现！",
        price: 12,
        img: "/images/meals/7.png"
      }
    ],
    keyword: ""
  }),
  getters: {
    //将数组根据关键词keyword过滤，留下含有keyword的组成新的数组
    filterHamburger: (state) => {
      return state.data.filter((item) => item.title.indexOf(state.keyword) != -1)
    },
    //获取选购商品的信息
    cartGoods: (state) => {
      return state.data.filter((item) => item.count > 0)
    },
    //商品数量
    totalGoods: (state) => {
      //没有选购商品，直接返回
      if (state.cartGoods.length <= 0) return 0
      //返回商品的总数量
      return state.cartGoods.reduce(
        (result, item) => result + item.count, 0
      )
    },
    //商品总价格
    totalPrice: (state) => {
      if (state.cartGoods.length <= 0) return 0
      return state.cartGoods.reduce(
        (result, item) => result + item.count * item.price, 0
      )
    }
  },
  actions: {
    //将选购的商品数量加入购物车
    addHamburgerToCart(hamburger) {
      //判断是否选取
      if (isNaN(hamburger.count)) {
        hamburger.count = 0
      }
      hamburger.count++
    },
    //删减商品
    subHamburgerFromCart(hamburger) {
      if (isNaN(hamburger.count) || hamburger.count <= 0) {
        return
      }
      hamburger.count--
    },
    //清空购物车、
    clearCart() {
      this.cartGoods.forEach(item => item.count = 0)
    }
  }
})